#include <Arduino.h>
#include "DHT.h"
#include <WiFi.h>

#define DHTPIN 4 // Digital pin connected to the DHT sensor
#define RELAY_CH1_PIN 25
#define RELAY_CH2_PIN 26
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14 --
// Pin 15 can work but DHT must be disconnected during program upload.

// Uncomment whatever type you're using!
#define DHTTYPE DHT11 // DHT 11

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
DHT dht(DHTPIN, DHTTYPE);

const float targetTemperatureHigh = 10;
const float targetTemperatureLow = 9;

RTC_DATA_ATTR unsigned long lastSensorRead = 0;
RTC_DATA_ATTR int bootCount = 0;
RTC_DATA_ATTR float currentHumidity = 0;
RTC_DATA_ATTR float currentTemperature = 0;
RTC_DATA_ATTR boolean relayState = false;

RTC_DATA_ATTR int sensorErrors = 0;
const int maxSesnorErrors = 5;

// WiFi + HTTP Server
const char *ssid = "TemperatureSensor";
const char *password = "password123Hell";

// Set web server port number to 80
WiFiServer server(80);

void OnWiFiEvent(WiFiEvent_t event)
{
  switch (event)
  {
  case SYSTEM_EVENT_STA_CONNECTED:
    Serial.println("ESP32 Connected to WiFi Network");
    break;
  case SYSTEM_EVENT_AP_START:
    Serial.println("ESP32 soft AP started");
    break;
  case SYSTEM_EVENT_AP_STACONNECTED:
    Serial.println("Station connected to ESP32 soft AP");
    break;
  case SYSTEM_EVENT_AP_STADISCONNECTED:
    Serial.println("Station disconnected from ESP32 soft AP");
    break;
  default:
    break;
  }
}

void initWifi()
{
  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);

  Serial.print("Setting soft-AP configuration ... ");
  WiFi.onEvent(OnWiFiEvent);
  WiFi.softAP(ssid, password); /*ESP32 wifi set in Access Point mode*/

  IPAddress IP = WiFi.softAPIP();
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.print("IP address: ");
  Serial.println(IP);
  server.begin();
}

void updateRelayState()
{
  if (sensorErrors >= maxSesnorErrors)
  {
    relayState = false;
    //Serial.println("Bad sensor data, Relay - off");
  }
  else
  {
    if (relayState && currentTemperature >= targetTemperatureHigh)
    {
      relayState = false;
      Serial.println("Relay - off");
    }
    else if (!relayState && currentTemperature <= targetTemperatureLow)
    {
      relayState = true;
      Serial.println("Relay - on");
    }
  }

  // Pull down mode, LOW value on module (Mostly because it has test buttons wired that way)
  digitalWrite(RELAY_CH1_PIN, !relayState ? HIGH : LOW);
  digitalWrite(RELAY_CH2_PIN, !relayState ? HIGH : LOW);
}

void setup()
{
  pinMode(RELAY_CH1_PIN, OUTPUT);
  pinMode(RELAY_CH2_PIN, OUTPUT);
  updateRelayState();

  // Serial.begin(9600);
  Serial.begin(115200);

  dht.begin();

  initWifi();
}

void humiditySensor()
{
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f))
  {
    sensorErrors += 1;
    Serial.println("Failed to read from DHT sensor!");
    return;
  }
  else
  {
    sensorErrors = 0;
  }

  currentHumidity = h;
  currentTemperature = t;

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.print("%  Temperature: ");
  Serial.print(t);
  Serial.print("°C ");
  Serial.print(f);
  Serial.print("°F  Heat index: ");
  Serial.print(hic);
  Serial.print("°C ");
  Serial.print(hif);
  Serial.println("°F");
}

String header;

// Current time
unsigned long currentTime = millis();
// Previous time
unsigned long previousTime = 0;
// Define timeout time in milliseconds (example: 2000ms = 2s)
const long timeoutTime = 2000;
void doWifiBussiness()
{
  WiFiClient client = server.available(); // Listen for incoming clients

  if (client)
  { // If a new client connects,
    currentTime = millis();
    previousTime = currentTime;
    Serial.println("New Client."); // print a message out in the serial port
    String currentLine = "";       // make a String to hold incoming data from the client
    while (client.connected() && currentTime - previousTime <= timeoutTime)
    { // loop while the client's connected
      currentTime = millis();
      if (client.available())
      {                         // if there's bytes to read from the client,
        char c = client.read(); // read a byte, then
        Serial.write(c);        // print it out the serial monitor
        header += c;
        if (c == '\n')
        { // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0)
          {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();

            // turns the GPIOs on and off
            if (header.indexOf("GET /action") >= 0)
            {
              relayState = !relayState;
              updateRelayState();
              Serial.println("Action - switching relay state!!!!");
            }

            // Display the HTML web page
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            // CSS to style the on/off buttons
            // Feel free to change the background-color and font-size attributes to fit your preferences
            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            client.println(".button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".button2 {background-color: #555555;}</style></head>");

            // Web Page Heading
            client.println("<body><h1>ESP32 Web Server</h1>");

            client.println("<p>Relay state: ");
            client.println(relayState ? "On" : "Off");
            client.println("</p>");

            char result[8];                            // Buffer big enough for 7-character float
            dtostrf(currentTemperature, 6, 2, result); // Leave room for too large numbers!
            client.println("<p>Current temperature: ");
            client.println(result);
            client.println("</p>");
            dtostrf(currentHumidity, 6, 2, result); // Leave room for too large numbers!
            client.println("<p>Current humidity: ");
            client.println(result);
            client.println("</p>");
            client.println("<p>Sensor error count: ");
            client.println(sensorErrors, 10);
            client.println("</p>");

            // Needs special testing mode to override reading from sensor
            // client.println("<p><a href=\"/action\"><button class=\"button\">Switch relay</button></a></p>");

            client.println("</body>");

            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          }
          else
          { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        }
        else if (c != '\r')
        {                   // if you got anything else but a carriage return character,
          currentLine += c; // add it to the end of the currentLine
        }
      }
    }

    header = "";

    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}

void doEvery(long intervalTime, void (*action)())
{
  unsigned long currentMillis = millis();
  if (lastSensorRead == 0 || currentMillis - lastSensorRead >= intervalTime)
  {
    action();
    lastSensorRead = currentMillis;
  }
}

void loop()
{
  // Wait a few seconds between measurements.
  doEvery(2000, []()
          { humiditySensor(); });
  updateRelayState();
  doWifiBussiness();
}
